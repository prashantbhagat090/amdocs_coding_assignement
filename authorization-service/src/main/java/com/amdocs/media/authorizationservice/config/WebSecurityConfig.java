package com.amdocs.media.authorizationservice.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.client.RestTemplate;
import com.amdocs.media.authorizationservice.constant.GlobalConstant;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private JwtTokenProvider jwtTokenProvider;

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.cors().and().csrf().disable();
		http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		http.authorizeRequests().antMatchers(GlobalConstant.LOGIN_SLASH).permitAll()
				.antMatchers(GlobalConstant.ASSIGNEMENT_LOGIN).permitAll().antMatchers(GlobalConstant.SIGN_OUT)
				.permitAll().antMatchers(GlobalConstant.VALID_TOKEN_URL).permitAll()
				.antMatchers(GlobalConstant.LOGIN_TOKEN_URL).permitAll().antMatchers(GlobalConstant.SIGNUP_URL)
				.permitAll().antMatchers(GlobalConstant.ASSIGNEMENT_SIGN_UP).permitAll().anyRequest().authenticated();
		http.exceptionHandling().accessDeniedPage(GlobalConstant.SIGN_IN);
		http.apply(new JwtTokenFilterConfigurer(jwtTokenProvider));
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers(GlobalConstant.EXTRA_SLASH)//
				.antMatchers(GlobalConstant.EUEREKA_SLASH)//
				.antMatchers(GlobalConstant.SIGN_OUT)//
				.antMatchers(GlobalConstant.VALID_TOKEN_URL)//
				.antMatchers(GlobalConstant.LOGIN_TOKEN_URL)//
				.antMatchers(GlobalConstant.SIGNUP_URL)//
				.antMatchers(GlobalConstant.H2_CONSOLE)//
				.antMatchers(HttpMethod.OPTIONS, GlobalConstant.SINGLE_EXTRA_SLASH); // Request type options should be
																						// allowed.
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder(12);
	}

	@Bean
	public AuthenticationManager customAuthenticationManager() throws Exception {
		return authenticationManager();
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
}
