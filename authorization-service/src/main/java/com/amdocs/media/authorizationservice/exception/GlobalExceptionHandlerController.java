package com.amdocs.media.authorizationservice.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import com.amdocs.media.authorizationservice.constant.GlobalConstant;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestControllerAdvice
public class GlobalExceptionHandlerController {
	private final Logger LOG = LoggerFactory.getLogger(getClass());

	@ExceptionHandler(CustomException.class)
	public void handleCustomException(HttpServletResponse res, CustomException e) throws IOException {
		LOG.error(GlobalConstant.ERROR, e);
		res.sendError(e.getHttpStatus().value(), e.getMessage());
	}

	@ExceptionHandler(AccessDeniedException.class)
	public void handleAccessDeniedException(HttpServletResponse res, AccessDeniedException e) throws IOException {
		LOG.error(GlobalConstant.ERROR, e);
		res.sendError(HttpStatus.FORBIDDEN.value(), GlobalConstant.ACCESS_DENIED);
	}

	@ExceptionHandler(IllegalArgumentException.class)
	public void handleIllegalArgumentException(HttpServletResponse res, IllegalArgumentException e) throws IOException {
		LOG.error(GlobalConstant.ERROR, e);
		res.sendError(HttpStatus.BAD_REQUEST.value(), GlobalConstant.SOMETHING_WENT_WRONG);
	}

	@ExceptionHandler(Exception.class)
	public void handleException(HttpServletResponse res, Exception e) throws IOException {
		LOG.error(GlobalConstant.ERROR, e);
		res.sendError(HttpStatus.BAD_REQUEST.value(), GlobalConstant.SOMETHING_WENT_WRONG);
	}

}
