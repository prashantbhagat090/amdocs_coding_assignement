package com.amdocs.media.authorizationservice.config;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import com.amdocs.media.authorizationservice.constant.GlobalConstant;
import com.amdocs.media.authorizationservice.exception.CustomException;
import com.amdocs.media.authorizationservice.model.CustomUserDetails;
import com.amdocs.media.authorizationservice.model.JwtToken;
import com.amdocs.media.authorizationservice.model.Profile;
import com.amdocs.media.authorizationservice.model.User;
import com.amdocs.media.authorizationservice.repository.JwtTokenRepository;
import com.amdocs.media.authorizationservice.repository.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class JwtTokenProvider {
	private static final String AUTH = "auth";
	private static final String AUTHORIZATION = "Authorization";
	private String secretKey = "secret-key";
	private long validityInMilliseconds = 3600000; // 1h
	private static final Logger LOG = LoggerFactory.getLogger(JwtTokenProvider.class);

	@Autowired
	private JwtTokenRepository jwtTokenRepository;

	@Autowired
	private UserRepository userRepository;

	@PostConstruct
	protected void init() {
		secretKey = Base64.getEncoder().encodeToString(secretKey.getBytes());
	}

	public String createToken(String username, List<String> roles) {

		Claims claims = Jwts.claims().setSubject(username);
		claims.put(AUTH, roles);

		Date now = new Date();
		Date validity = new Date(now.getTime() + validityInMilliseconds);

		String token = Jwts.builder()//
				.setClaims(claims)//
				.setIssuedAt(now)//
				.setExpiration(validity)//
				.signWith(SignatureAlgorithm.HS256, secretKey)//
				.compact();
		jwtTokenRepository.save(new JwtToken(token, roles.get(0)));
		return token;
	}

	public String resolveToken(HttpServletRequest req) {
		String bearerToken = req.getHeader(AUTHORIZATION);
		if (bearerToken != null && bearerToken.startsWith(GlobalConstant.BEARER)) {
			return bearerToken.substring(7, bearerToken.length());
		}
		return null;
	}

	public Profile resolvePhoneNumber(HttpServletRequest req) throws IOException {
		try {
			String test = GlobalConstant.EMPTY_STRING;
			ObjectReader or = new ObjectMapper().readerFor(Profile.class);
			test = req.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
			if ((!(test.equals(GlobalConstant.EMPTY_STRING)))) {
				Profile json = (Profile) or.readValue(test);
				return json;
			}
			return new Profile();
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error(GlobalConstant.INVALID_PHONE_NUMBER);
			return new Profile();
		}
	}

	public boolean authenticateTokenAndRole(HttpServletRequest req, String token) throws IOException {
		Profile profile = resolvePhoneNumber(req);
		JwtToken jwtToken = null;
		try {
			jwtToken = jwtTokenRepository.findByToken(token);
		} catch (Exception e) {
			LOG.error(GlobalConstant.INVALID_JWT_TOKEN);
		}

		User usr = userRepository.getUserByPhoneNumber(profile.getPhone_number());
		if (usr != null && jwtToken != null) {
			if (!(GlobalConstant.ROLE_ + usr.getUsername()).equals(jwtToken.getRole())) {
				LOG.error(GlobalConstant.INVALID_JWT_TOKEN);
				throw new CustomException(GlobalConstant.INVALID_JWT_TOKEN, HttpStatus.UNAUTHORIZED);
			}
		}

		String userName = getUsername(token);
		User usr2 = userRepository.getUserByUsername(userName);
		if (usr2 != null && profile != null) {
			if (!(usr2.getPhone_number().equals(profile.getPhone_number()))) {
				LOG.error(GlobalConstant.INVALID_PHONE_NUMBER);
				return false;
			}
		}
		return true;
	}

	public boolean validateToken(String token) throws JwtException, IllegalArgumentException {
		Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);
		return true;
	}

	public boolean validateRole(String token) throws JwtException, IllegalArgumentException {
		Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);
		return true;
	}

	public boolean isTokenPresentInDB(String token) {
		JwtToken jwtToken = null;
		try {
			jwtToken = jwtTokenRepository.findByToken(token);
		} catch (Exception e) {
			LOG.error(GlobalConstant.JWT_TOKEN_NOT_FOUND);
			return false;
		}
		if (jwtToken != null && jwtToken.getId() != null) {
			return true;
		}
		return false;
	}

	public UserDetails getUserDetails(String token) {
		String userName = getUsername(token);
		List<String> roleList = getRoleList(token);
		UserDetails userDetails = new CustomUserDetails(userName, roleList.toArray(new String[roleList.size()]));
		return userDetails;
	}

	@SuppressWarnings("unchecked")
	public List<String> getRoleList(String token) {
		return (List<String>) Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody().get(AUTH);
	}

	public String getUsername(String token) {
		return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody().getSubject();
	}

	public Authentication getAuthentication(String token) {
		UserDetails userDetails = getUserDetails(token);
		return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
	}

}
