package com.amdocs.media.authorizationservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.amdocs.media.authorizationservice.constant.GlobalConstant;
import com.amdocs.media.authorizationservice.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

	@Query(value = GlobalConstant.GET_USER_BY_USERNAME_QUERY, nativeQuery = true)
	User getUserByUsername(@Param(GlobalConstant.USERNAME) String username);

	@Query(value = GlobalConstant.GET_USER_BY_PHONE_NUMBER_QUERY, nativeQuery = true)
	User getUserByPhoneNumber(@Param(GlobalConstant.PHONE_NUMBER) Long phone_number);
}
