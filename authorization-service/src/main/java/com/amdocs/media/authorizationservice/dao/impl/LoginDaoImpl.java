package com.amdocs.media.authorizationservice.dao.impl;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;
import com.amdocs.media.authorizationservice.config.JwtTokenProvider;
import com.amdocs.media.authorizationservice.constant.GlobalConstant;
import com.amdocs.media.authorizationservice.dao.LoginDao;
import com.amdocs.media.authorizationservice.exception.CustomException;
import com.amdocs.media.authorizationservice.model.JwtToken;
import com.amdocs.media.authorizationservice.model.User;
import com.amdocs.media.authorizationservice.repository.JwtTokenRepository;
import com.amdocs.media.authorizationservice.repository.UserRepository;

@Repository
public class LoginDaoImpl implements LoginDao {

	private static final Logger LOG = LoggerFactory.getLogger(LoginDaoImpl.class);

	@Autowired
	private PasswordEncoder passwordEncoder;
	@Autowired
	private JwtTokenProvider jwtTokenProvider;
	@Autowired
	private AuthenticationManager authenticationManager;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private JwtTokenRepository jwtTokenRepository;

	@Override
	public String login(String username, String password) {
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
			User user = userRepository.getUserByUsername(username);
			if (user == null || user.getRole() == null || user.getRole().isEmpty()) {
				throw new CustomException(GlobalConstant.INVALID_USERNAME_AND_PASSWORD, HttpStatus.UNAUTHORIZED);
			}
			List<String> rolelist = new ArrayList<>();
			rolelist.add(user.getRole());
			String token = jwtTokenProvider.createToken(username, rolelist);
			return token;

		} catch (AuthenticationException e) {
			LOG.error(e.getMessage());
			throw new CustomException(GlobalConstant.INVALID_USERNAME_AND_PASSWORD, HttpStatus.UNAUTHORIZED);
		}
	}

	@Override
	public User saveUser(User user) {
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		user.setRole(GlobalConstant.ROLE_ + user.getUsername());
		return userRepository.save(user);
	}

	@Override
	public boolean logout(String token, String role) {
		jwtTokenRepository.delete(new JwtToken(token, role));
		return true;
	}

	@Override
	public Boolean isValidToken(String token) {
		return jwtTokenProvider.validateToken(token);
	}

	@Override
	public String createNewToken(String token) {
		String username = jwtTokenProvider.getUsername(token);
		List<String> roleList = jwtTokenProvider.getRoleList(token);
		String newToken = jwtTokenProvider.createToken(username, roleList);
		return newToken;
	}

	@Override
	public boolean checkIfUserAlreadyExist(User user) {
		User userFromDb = userRepository.getUserByUsername(user.getUsername());
		if (userFromDb != null) {
			throw new CustomException(GlobalConstant.USERNAME_ALREADY_EXIST, HttpStatus.UNAUTHORIZED);
		}
		User userFromDb2 = userRepository.getUserByPhoneNumber(user.getPhone_number());
		if (userFromDb2 != null) {
			throw new CustomException(GlobalConstant.PHONE_NUMBER_ALREADY_EXIST, HttpStatus.UNAUTHORIZED);
		}
		if (user != null && (user.getPhone_number() == null || user.getPhone_number() < 10)) {
			throw new CustomException(GlobalConstant.PLEASE_ENTER_CORRECT_PHONE_NO, HttpStatus.BAD_REQUEST);
		}
		return false;

	}

}
