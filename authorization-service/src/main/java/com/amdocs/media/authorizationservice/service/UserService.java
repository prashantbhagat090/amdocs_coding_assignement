package com.amdocs.media.authorizationservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import com.amdocs.media.authorizationservice.constant.GlobalConstant;
import com.amdocs.media.authorizationservice.exception.CustomException;
import com.amdocs.media.authorizationservice.model.CustomUserDetails;
import com.amdocs.media.authorizationservice.model.User;
import com.amdocs.media.authorizationservice.repository.UserRepository;

@Service
public class UserService implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		User user = userRepository.getUserByUsername(username);
		if (user == null || user.getRole() == null || user.getRole().isEmpty()) {
			throw new CustomException(GlobalConstant.INVALID_USERNAME_AND_PASSWORD, HttpStatus.UNAUTHORIZED);
		}
		String[] authorities = { user.getRole() };
		CustomUserDetails userDetails = new CustomUserDetails(user.getUsername(), user.getPassword(), user.getActive(),
				user.isLoacked(), user.isExpired(), user.isEnabled(), authorities);
		return userDetails;
	}

}
