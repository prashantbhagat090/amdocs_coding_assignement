package com.amdocs.media.authorizationservice.model;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Table(name = "user")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String username;

	private Long phone_number;

	private String password;

	private String name;

	private String lastName;

	private Integer active = 1;

	private boolean isLoacked = false;

	private boolean isExpired = false;

	private boolean isEnabled = true;

	private String role;

}
