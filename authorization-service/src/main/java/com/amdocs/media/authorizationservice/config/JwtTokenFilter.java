package com.amdocs.media.authorizationservice.config;

import io.jsonwebtoken.JwtException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;
import com.amdocs.media.authorizationservice.constant.GlobalConstant;
import com.amdocs.media.authorizationservice.exception.CustomException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JwtTokenFilter extends GenericFilterBean {
	private JwtTokenProvider jwtTokenProvider;
	private static final Logger LOG = LoggerFactory.getLogger(JwtTokenFilter.class);

	public JwtTokenFilter(JwtTokenProvider jwtTokenProvider) {
		this.jwtTokenProvider = jwtTokenProvider;
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain filterChain)
			throws IOException, ServletException {
		HttpServletResponse response = (HttpServletResponse) res;
		HttpServletRequest request = new MultiReadRequestWrapper((HttpServletRequest) req);
		String token = jwtTokenProvider.resolveToken((HttpServletRequest) req);
		if (token != null) {
			if (!jwtTokenProvider.isTokenPresentInDB(token)) {
				response.sendError(HttpServletResponse.SC_UNAUTHORIZED, GlobalConstant.INVALID_JWT_TOKEN);
				LOG.error(GlobalConstant.INVALID_JWT_TOKEN);
				throw new CustomException(GlobalConstant.INVALID_JWT_TOKEN, HttpStatus.UNAUTHORIZED);
			}
			try {
				jwtTokenProvider.validateToken(token);
			} catch (JwtException | IllegalArgumentException e) {
				response.sendError(HttpServletResponse.SC_UNAUTHORIZED, GlobalConstant.INVALID_JWT_TOKEN);
				LOG.error(GlobalConstant.INVALID_JWT_TOKEN);
				throw new CustomException(GlobalConstant.INVALID_JWT_TOKEN, HttpStatus.UNAUTHORIZED);
			}
			if (!(jwtTokenProvider.authenticateTokenAndRole(request, token))) {
				response.sendError(HttpServletResponse.SC_UNAUTHORIZED, GlobalConstant.INVALID_PHONE_NUMBER);
				LOG.error(GlobalConstant.INVALID_PHONE_NUMBER);
				throw new CustomException(GlobalConstant.INVALID_PHONE_NUMBER, HttpStatus.UNAUTHORIZED);
				 
			}
			Authentication auth = token != null ? jwtTokenProvider.getAuthentication(token) : null;
			SecurityContextHolder.getContext().setAuthentication(auth);
		}
		filterChain.doFilter(request, res);
	}
}
