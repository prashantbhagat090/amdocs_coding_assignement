package com.amdocs.media.authorizationservice.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.amdocs.media.authorizationservice.dao.LoginDao;
import com.amdocs.media.authorizationservice.model.User;
import com.amdocs.media.authorizationservice.service.LoginService;

@Service
public class LoginServiceImpl implements LoginService {

	@Autowired
	private LoginDao loginDao;

	@Override
	public String login(String username, String password) {
		return loginDao.login(username, password);
	}

	@Override
	public User saveUser(User user) {
		return loginDao.saveUser(user);
	}

	@Override
	public boolean logout(String token, String role) {
		return loginDao.logout(token, role);
	}

	@Override
	public Boolean isValidToken(String token) {
		return loginDao.isValidToken(token);
	}

	@Override
	public String createNewToken(String token) {
		return loginDao.createNewToken(token);
	}

	@Override
	public boolean checkIfUserAlreadyExist(User user) {
		return loginDao.checkIfUserAlreadyExist(user);
	}
}
