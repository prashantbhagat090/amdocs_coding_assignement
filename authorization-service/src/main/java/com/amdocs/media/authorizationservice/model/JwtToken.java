package com.amdocs.media.authorizationservice.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import com.amdocs.media.authorizationservice.constant.GlobalConstant;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = GlobalConstant.JWT_TOKEN)
public class JwtToken {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String token;

	private String role;

	public JwtToken(String token, String role) {
		this.token = token;
		this.role = role;
	}

	public JwtToken() {
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
