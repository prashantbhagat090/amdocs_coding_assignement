package com.amdocs.media.authorizationservice.constant;

public class GlobalConstant {
	
	public static final String JWT_TOKEN="jwt_token";
	public static final String INVALID_JWT_TOKEN="Invalid JWT token";
	public static final String BEARER="Bearer ";
	public static final String POST="POST";
	public static final String ROLE_="role_";
	public static final String INVALID_USERNAME_AND_PASSWORD="Invalid username or password.";
	public static final String LOGIN_SLASH="/**/login/**";
	public static final String ASSIGNEMENT_LOGIN="/assignement/login**";
	public static final String ASSIGNEMENT_SIGN_UP="/assignement/signup**";
	public static final String SIGN_IN="/signin";
	public static final String EXTRA_SLASH="/*/";
	public static final String EUEREKA_SLASH="/eureka/**";
	public static final String H2_CONSOLE="/h2-console/**";
	public static final String SINGLE_EXTRA_SLASH="/**";
	public static final String ASSIGNEMENT_URL="/assignement";
	public static final String LOGIN_URL="/login";
	public static final String STAR="*";
	public static final String CONTENT_TYPE="Content-Type";
	public static final String ACCEPT=" Accept";
	public static final String X_REQUESTED_WITH="X-Requested-With";
	public static final String AUTHORIZATION="Authorization";
	public static final String SIGN_OUT="/signout";
	public static final String LOGGED_OUT="logged out";
	public static final String LOGGED_FAILED="Logout Failed";
	public static final String ROLE="role";
	public static final String VALID_TOKEN_URL="/valid/token";
	public static final String LOGIN_TOKEN_URL="/login/token";
	public static final String SIGNUP_URL="/signup";
	public static final String REGISTERED_SUCCESSFULLY="You have been registred successfully!";
	public static final String ERROR="ERROR";
	public static final String ACCESS_DENIED="Access denied";
	public static final String SOMETHING_WENT_WRONG="Something went wrong";
	public static final String SELECT_JWT_TOKEN_QUERY="select * from jwt_token where token=:token";
	public static final String TOKEN="token";
	public static final String GET_USER_BY_USERNAME_QUERY="select * from user where username=:username";
	public static final String GET_USER_BY_PHONE_NUMBER_QUERY="select * from user where phone_number=:phone_number";
	public static final String USERNAME="username";
	public static final String PHONE_NUMBER="phone_number";
	public static final String USERNAME_ALREADY_EXIST="Username already exist";
	public static final String PHONE_NUMBER_ALREADY_EXIST="Phone no. already exist";
	public static final String PLEASE_ENTER_CORRECT_PHONE_NO="Please enter correc phone no";
	public static final String JWT_TOKEN_NOT_FOUND="Jwt token not found in db";
	public static final String REACHED_LOGIN_METHOD="Reached login method";
	public static final String REACHED_LOGOUT_METHOD="Reached log out method";
	public static final String EMPTY_STRING="";
	public static final String VALIDATE_LOGIN="Reached validate login method";
	public static final String CREATE_NEW_TOKEN="Reached create new token method";
	public static final String REACHED_SING_UP="Reached sign up method";
	public static final String INVALID_PHONE_NUMBER="Invalid phone no";
	public static final String PASS_VALID_NUMBER="Please pass valid phone no.";
	
	
	

}
