package com.amdocs.media.authorizationservice.controller;

import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.amdocs.media.authorizationservice.constant.GlobalConstant;
import com.amdocs.media.authorizationservice.model.AuthResponse;
import com.amdocs.media.authorizationservice.model.LoginRequest;
import com.amdocs.media.authorizationservice.model.User;
import com.amdocs.media.authorizationservice.service.LoginService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = GlobalConstant.STAR)
@RestController
@RequestMapping(GlobalConstant.ASSIGNEMENT_URL)
public class LoginController {

	private static final Logger LOG = LoggerFactory.getLogger(LoginController.class);

	@Autowired
	private LoginService iLoginService;

	/**
	 * @param loginRequest Allow authenticated user to access
	 * @return ResponseEntity<AuthResponse> returned object contains access token
	 */
	@CrossOrigin(GlobalConstant.STAR)
	@PostMapping(GlobalConstant.LOGIN_URL)
	@ResponseBody
	public ResponseEntity<AuthResponse> login(@RequestBody LoginRequest loginRequest) {
		LOG.info(GlobalConstant.REACHED_LOGIN_METHOD);
		String token = iLoginService.login(loginRequest.getUsername(), loginRequest.getPassword());
		HttpHeaders headers = new HttpHeaders();
		List<String> headerlist = new ArrayList<>();
		List<String> exposeList = new ArrayList<>();
		headerlist.add(GlobalConstant.CONTENT_TYPE);
		headerlist.add(GlobalConstant.ACCEPT);
		headerlist.add(GlobalConstant.X_REQUESTED_WITH);
		headerlist.add(GlobalConstant.AUTHORIZATION);
		headers.setAccessControlAllowHeaders(headerlist);
		exposeList.add(GlobalConstant.AUTHORIZATION);
		headers.setAccessControlExposeHeaders(exposeList);
		headers.set(GlobalConstant.AUTHORIZATION, token);
		return new ResponseEntity<AuthResponse>(new AuthResponse(token), headers, HttpStatus.CREATED);
	}

	/**
	 * 
	 * @param token
	 * @param username
	 * @return ResponseEntity<AuthResponse> will remove jwt access token from
	 *         database
	 */
	@CrossOrigin(GlobalConstant.STAR)
	@PostMapping(GlobalConstant.SIGN_OUT)
	@ResponseBody
	public ResponseEntity<AuthResponse> logout(@RequestHeader(value = GlobalConstant.AUTHORIZATION) String token,
			@RequestHeader(value = GlobalConstant.USERNAME) String username) {
		LOG.info(GlobalConstant.REACHED_LOGOUT_METHOD);
		HttpHeaders headers = new HttpHeaders();
		if (iLoginService.logout(token, GlobalConstant.ROLE_ + username)) {
			headers.remove(GlobalConstant.AUTHORIZATION);
			return new ResponseEntity<AuthResponse>(new AuthResponse(GlobalConstant.LOGGED_OUT), headers,
					HttpStatus.CREATED);
		}
		return new ResponseEntity<AuthResponse>(new AuthResponse(GlobalConstant.LOGGED_FAILED), headers,
				HttpStatus.NOT_MODIFIED);
	}

	/**
	 *
	 * @param token
	 * @return boolean. if request reach here it means it is a valid token.
	 */
	@PostMapping(GlobalConstant.VALID_TOKEN_URL)
	@ResponseBody
	public Boolean isValidToken(@RequestHeader(value = GlobalConstant.AUTHORIZATION) String token) {
		LOG.info(GlobalConstant.VALIDATE_LOGIN);
		return true;
	}

	/**
	 * 
	 * @param token
	 * @return ResponseEntity<AuthResponse> will return new access token
	 */
	@PostMapping(GlobalConstant.LOGIN_TOKEN_URL)
	@CrossOrigin(GlobalConstant.STAR)
	@ResponseBody
	public ResponseEntity<AuthResponse> createNewToken(
			@RequestHeader(value = GlobalConstant.AUTHORIZATION) String token) {
		LOG.info(GlobalConstant.CREATE_NEW_TOKEN);
		String newToken = iLoginService.createNewToken(token);
		HttpHeaders headers = new HttpHeaders();
		List<String> headerList = new ArrayList<>();
		List<String> exposeList = new ArrayList<>();
		headerList.add(GlobalConstant.CONTENT_TYPE);
		headerList.add(GlobalConstant.ACCEPT);
		headerList.add(GlobalConstant.X_REQUESTED_WITH);
		headerList.add(GlobalConstant.AUTHORIZATION);
		headers.setAccessControlAllowHeaders(headerList);
		exposeList.add(GlobalConstant.AUTHORIZATION);
		headers.setAccessControlExposeHeaders(exposeList);
		headers.set(GlobalConstant.AUTHORIZATION, newToken);
		return new ResponseEntity<AuthResponse>(new AuthResponse(newToken), headers, HttpStatus.CREATED);
	}

	/**
	 * 
	 * @param user Will allow to add new user
	 * @return ResponseEntity<String>
	 */
	@CrossOrigin(GlobalConstant.STAR)
	@PostMapping(GlobalConstant.SIGNUP_URL)
	@ResponseBody
	public ResponseEntity<String> signup(@RequestBody User user) {
		LOG.info(GlobalConstant.REACHED_SING_UP);
		if (!iLoginService.checkIfUserAlreadyExist(user)) {
			iLoginService.saveUser(user);
		}
		return new ResponseEntity<String>(GlobalConstant.REGISTERED_SUCCESSFULLY, HttpStatus.CREATED);
	}
}
