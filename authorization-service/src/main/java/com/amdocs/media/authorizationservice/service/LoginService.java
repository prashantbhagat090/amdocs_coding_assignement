package com.amdocs.media.authorizationservice.service;

import com.amdocs.media.authorizationservice.model.User;

public interface LoginService {
	
	/**
	 * @param username
	 * @param password
	 * @return String will return access token
	 */
	String login(String username, String password);

	/**
	 * 
	 * @param user
	 * @return User will persist user in db and return response
	 */
	User saveUser(User user);

	/**
	 * 
	 * @param token
	 * @param role
	 * @return boolean will remove jwt access token from db
	 */
	boolean logout(String token, String role);

	/**
	 * 
	 * @param token
	 * @return Boolean will return true if token is valid
	 */
	Boolean isValidToken(String token);

	/**
	 * 
	 * @param token
	 * @return will create new token and return it
	 */
	String createNewToken(String token);

	/**
	 * 
	 * @param user
	 * @return true if user exist in db
	 */
	boolean checkIfUserAlreadyExist(User user);
}
