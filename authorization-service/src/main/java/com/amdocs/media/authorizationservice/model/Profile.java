package com.amdocs.media.authorizationservice.model;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Profile implements Serializable {

	 
	private static final long serialVersionUID = 1L;

	private Long profileDetailsId;

	private String address;

	private Long phone_number;

}
