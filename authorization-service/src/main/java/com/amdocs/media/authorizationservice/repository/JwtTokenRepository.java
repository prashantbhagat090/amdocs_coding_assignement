package com.amdocs.media.authorizationservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.amdocs.media.authorizationservice.constant.GlobalConstant;
import com.amdocs.media.authorizationservice.model.JwtToken;

@Repository
public interface JwtTokenRepository extends JpaRepository<JwtToken, Long> {

	@Query(value = GlobalConstant.SELECT_JWT_TOKEN_QUERY, nativeQuery = true)
	JwtToken findByToken(@Param(GlobalConstant.TOKEN) String token);
}
