package com.amdocs.media.authorizationservice.constant;

public class GlobalConstantTest {
	
	public static final String USERNAME="test";
	public static final String PASSWORD="pass";
	public static final String TOKEN="eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJwcmFzaGFudDUiLCJhdXRoIjpbInJvbGVfcHJhc2hhbnQ1Il0sImlhdCI6MTU4ODQ4NTQ0NywiZXhwIjoxNTg4NDg5MDQ3fQ._Z-QSt_JASgdmSdc87AK_ZgjttXojza8kPru126G-O0";
	public static final String ROLE = "role_admin";

}
