package com.amdocs.media.authorizationservice.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import com.amdocs.media.authorizationservice.constant.GlobalConstantTest;
import com.amdocs.media.authorizationservice.model.LoginRequest;
import com.amdocs.media.authorizationservice.model.User;
import com.amdocs.media.authorizationservice.service.LoginService;

@RunWith(MockitoJUnitRunner.class)
public class LoginControllerTest {

	@InjectMocks
	LoginController loginController;

	@Mock
	private LoginService iLoginService;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void loginTest() {
		LoginRequest loginRequest = Mockito.mock(LoginRequest.class);
		loginController.login(loginRequest);
	}

	@Test
	public void logoutTest() {
		String token = GlobalConstantTest.TOKEN;
		String username = GlobalConstantTest.USERNAME;
		loginController.logout(token, username);
	}

	@Test
	public void isValidTokenTest() {
		String token = GlobalConstantTest.TOKEN;
		loginController.isValidToken(token);
	}

	@Test
	public void createNewTokenTest() {
		String token = GlobalConstantTest.TOKEN;
		loginController.createNewToken(token);
	}

	@Test
	public void signup() {
		User user = Mockito.mock(User.class);
		loginController.signup(user);
	}

}
