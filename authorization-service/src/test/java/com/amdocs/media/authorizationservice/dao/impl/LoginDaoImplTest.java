package com.amdocs.media.authorizationservice.dao.impl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import com.amdocs.media.authorizationservice.config.JwtTokenProvider;
import com.amdocs.media.authorizationservice.constant.GlobalConstantTest;
import com.amdocs.media.authorizationservice.model.User;
import com.amdocs.media.authorizationservice.repository.JwtTokenRepository;
import com.amdocs.media.authorizationservice.repository.UserRepository;

@RunWith(MockitoJUnitRunner.class)
public class LoginDaoImplTest {

	@InjectMocks
	LoginDaoImpl loginDaoImpl;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Mock
	private PasswordEncoder passwordEncoder;
	@Mock
	private JwtTokenProvider jwtTokenProvider;
	@Mock
	private AuthenticationManager authenticationManager;
	@Mock
	private UserRepository userRepository;
	@Mock
	private JwtTokenRepository jwtTokenRepository;

	@Test
	public void loginTest() {
		String username = GlobalConstantTest.USERNAME;
		String password = GlobalConstantTest.PASSWORD;
		try {
			loginDaoImpl.login(username, password);
		} catch (Exception e) {
		}

	}

	@Test
	public void saveUserTest() {
		User user = Mockito.mock(User.class);
		loginDaoImpl.saveUser(user);
	}

	@Test
	public void logoutTest() {
		String token = GlobalConstantTest.TOKEN;
		String role = GlobalConstantTest.ROLE;
		loginDaoImpl.logout(token, role);
	}

	@Test
	public void isValidTokenTest() {
		String token = GlobalConstantTest.TOKEN;
		loginDaoImpl.isValidToken(token);
	}

	@Test
	public void createNewTokenTest() {
		String token = GlobalConstantTest.TOKEN;
		loginDaoImpl.createNewToken(token);
	}

	@Test
	public void checkIfUserAlreadyExistTest() {
		try {
			User user = Mockito.mock(User.class);
			loginDaoImpl.checkIfUserAlreadyExist(user);
		} catch (Exception e) {

		}

	}

}
