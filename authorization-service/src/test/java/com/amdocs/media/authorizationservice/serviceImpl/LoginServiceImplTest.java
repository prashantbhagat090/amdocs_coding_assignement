package com.amdocs.media.authorizationservice.serviceImpl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import com.amdocs.media.authorizationservice.constant.GlobalConstantTest;
import com.amdocs.media.authorizationservice.dao.LoginDao;
import com.amdocs.media.authorizationservice.model.User;
import com.amdocs.media.authorizationservice.serviceImpl.LoginServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class LoginServiceImplTest {

	@InjectMocks
	LoginServiceImpl loginServiceImpl;

	@Mock
	private LoginDao loginDao;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void loginTest() {
		String username = GlobalConstantTest.USERNAME;
		String password = GlobalConstantTest.PASSWORD;
		loginServiceImpl.login(username, password);
	}

	@Test
	public void saveUserTest() {
		User user = Mockito.mock(User.class);
		loginServiceImpl.saveUser(user);
	}

	@Test
	public void logoutTest() {
		String token = GlobalConstantTest.TOKEN;
		String role = GlobalConstantTest.ROLE;
		loginServiceImpl.logout(token, role);
	}

	@Test
	public void isValidTokenTest() {
		String token = GlobalConstantTest.TOKEN;
		loginServiceImpl.isValidToken(token);
	}

	@Test
	public void createNewTokenTest() {
		String token = GlobalConstantTest.TOKEN;
		loginServiceImpl.createNewToken(token);
	}

	@Test
	public void checkIfUserAlreadyExistTest() {
		User user = Mockito.mock(User.class);
		loginServiceImpl.checkIfUserAlreadyExist(user);
	}

}
