package com.amdocs.media.profileservice.serviceImpl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import com.amdocs.media.profileservice.dao.ProfileDao;
import com.amdocs.media.profileservice.model.ProfileDetails;

@RunWith(MockitoJUnitRunner.class)
public class ProfileServiceImplTest {

	@InjectMocks
	ProfileServiceImpl profileServiceImpl;

	@Mock
	private ProfileDao profileDao;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void saveProfileTest() {
		ProfileDetails profileDetails = Mockito.mock(ProfileDetails.class);
		profileServiceImpl.saveProfile(profileDetails);
	}

	@Test
	public void deleteProfileTest() {
		ProfileDetails profileDetails = Mockito.mock(ProfileDetails.class);
		profileServiceImpl.deletProfile(profileDetails);
	}

}
