package com.amdocs.media.profileservice;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@RunWith(MockitoJUnitRunner.class)
public class ProfileServiceApplicationTest {

	@Test
	public void contextLoads() {
	}

}
