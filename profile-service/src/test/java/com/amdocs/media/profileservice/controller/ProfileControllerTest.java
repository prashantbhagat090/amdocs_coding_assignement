package com.amdocs.media.profileservice.controller;

import javax.servlet.http.HttpServletRequest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpHeaders;
import com.amdocs.media.profileservice.model.ProfileDetails;
import com.amdocs.media.profileservice.producer.ProfileProducer;
import com.amdocs.media.profileservice.service.ProfileService;
import com.fasterxml.jackson.core.JsonProcessingException;

@RunWith(MockitoJUnitRunner.class)
public class ProfileControllerTest {

	@InjectMocks
	ProfileController profileController;

	@Mock
	private ProfileService profileService;

	@Mock
	private ProfileProducer profileProducer;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void updateProfileTest() throws JsonProcessingException {
		HttpHeaders headers = Mockito.mock(HttpHeaders.class);
		ProfileDetails profileDetails = Mockito.mock(ProfileDetails.class);
		HttpServletRequest httpServletRequest = Mockito.mock(HttpServletRequest.class);
		profileController.updateProfile(headers, profileDetails, httpServletRequest);
	}

	@Test
	public void putProfileTest() throws JsonProcessingException {
		HttpHeaders headers = Mockito.mock(HttpHeaders.class);
		ProfileDetails profileDetails = Mockito.mock(ProfileDetails.class);
		HttpServletRequest httpServletRequest = Mockito.mock(HttpServletRequest.class);
		try {
			profileController.putProfile(headers, profileDetails, httpServletRequest);
		} catch (Exception e) {
		}
	}

	@Test
	public void deleteProfileTest() throws JsonProcessingException {
		HttpHeaders headers = Mockito.mock(HttpHeaders.class);
		ProfileDetails profileDetails = Mockito.mock(ProfileDetails.class);
		HttpServletRequest httpServletRequest = Mockito.mock(HttpServletRequest.class);
		try {
			profileController.deleteProfile(headers, profileDetails, httpServletRequest);
		} catch (Exception e) {
		}

	}

}
