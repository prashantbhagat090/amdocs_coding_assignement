package com.amdocs.media.profileservice.daoimpl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import com.amdocs.media.profileservice.daoImpl.ProfileDaoImpl;
import com.amdocs.media.profileservice.model.ProfileDetails;
import com.amdocs.media.profileservice.repository.ProfileRepository;

@RunWith(MockitoJUnitRunner.class)
public class ProfileDaoImplTest {

	@InjectMocks
	ProfileDaoImpl profileDaoImpl;

	@Mock
	private ProfileRepository profileRepository;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void saveProfileTest() {
		ProfileDetails profileDetails = Mockito.mock(ProfileDetails.class);
		profileDaoImpl.saveProfile(profileDetails);
	}

	@Test
	public void deleteProfileTest() {
		ProfileDetails profileDetails = Mockito.mock(ProfileDetails.class);
		profileDaoImpl.deletProfile(profileDetails);
	}
}
