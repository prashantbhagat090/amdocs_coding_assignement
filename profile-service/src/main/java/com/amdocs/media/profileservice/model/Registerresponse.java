package com.amdocs.media.profileservice.model;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Registerresponse {

	private boolean status;
	private String reason;

}
