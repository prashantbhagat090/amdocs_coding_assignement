package com.amdocs.media.profileservice.controller;

import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.amdocs.media.profileservice.constant.GlobalConstant;
import com.amdocs.media.profileservice.model.ProfileDetails;
import com.amdocs.media.profileservice.model.Registerresponse;
import com.amdocs.media.profileservice.producer.ProfileProducer;
import com.amdocs.media.profileservice.service.ProfileService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

@CrossOrigin(origins = GlobalConstant.STAR)
@RequestMapping(value = GlobalConstant.SLASH)
@RestController
public class ProfileController {

	private static final Logger LOG = LoggerFactory.getLogger(ProfileController.class);

	@Autowired
	private ProfileService profileService;

	@Autowired
	private ProfileProducer profileProducer;

	/**
	 * @param headers
	 * @param profileDetails
	 * @param httpServletRequest
	 * @return ResponseEntity<String> will update profile synchronously and return
	 *         success response
	 * @throws JsonProcessingException
	 */
	@RequestMapping(value = GlobalConstant.PROFILE_URL, method = RequestMethod.POST)
	public ResponseEntity<String> updateProfile(@RequestHeader HttpHeaders headers,
			@RequestBody ProfileDetails profileDetails, HttpServletRequest httpServletRequest)
			throws JsonProcessingException {
		LOG.info(GlobalConstant.REACHED_UPDATE_PROFILE);
		Registerresponse registerresponse = profileService.saveProfile(profileDetails);
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		String json = ow.writeValueAsString(registerresponse);
		return new ResponseEntity<String>(json, HttpStatus.OK);

	}

	/**
	 * 
	 * @param headers
	 * @param profileDetails
	 * @param httpServletRequest
	 * @return ResponseEntity<String> Will update profile asynchronously with help
	 *         of kafka and return success response
	 * @throws JsonProcessingException
	 */
	@RequestMapping(value = GlobalConstant.PROFILE_URL, method = RequestMethod.PUT)
	public ResponseEntity<String> putProfile(@RequestHeader HttpHeaders headers,
			@RequestBody ProfileDetails profileDetails, HttpServletRequest httpServletRequest)
			throws JsonProcessingException {
		LOG.info(GlobalConstant.REACHED_PUT_PROFILE);
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		String json = ow.writeValueAsString(profileDetails);
		profileProducer.send(json, GlobalConstant.UPDATE_PROFILE);
		return new ResponseEntity<String>(GlobalConstant.PROFILE_UPDATE_MESSAGE, HttpStatus.OK);
	}

	/**
	 * 
	 * @param headers
	 * @param profileDetails
	 * @param httpServletRequest
	 * @return ResponseEntity<String> * Will delete profile asynchronously with help
	 *         of kafka and return success response
	 * @throws JsonProcessingException
	 */
	@RequestMapping(value = GlobalConstant.PROFILE_URL, method = RequestMethod.DELETE)
	public ResponseEntity<String> deleteProfile(@RequestHeader HttpHeaders headers,
			@RequestBody ProfileDetails profileDetails, HttpServletRequest httpServletRequest)
			throws JsonProcessingException {
		LOG.info(GlobalConstant.REACHED_DELETE_PROFILE);
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		String json = ow.writeValueAsString(profileDetails);
		profileProducer.send(json, GlobalConstant.DELETE_PROFILE);
		return new ResponseEntity<String>(GlobalConstant.PROFILE_DELETE_MESSAGE, HttpStatus.OK);
	}

}
