package com.amdocs.media.profileservice.consumer;

import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import com.amdocs.media.profileservice.constant.GlobalConstant;
import com.amdocs.media.profileservice.model.ProfileDetails;
import com.amdocs.media.profileservice.service.ProfileService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;

@Service
public class ProfileConsumer {

	@Autowired
	private ProfileService profileService;

	@KafkaListener(topics = GlobalConstant.UPDATE_PROFILE, groupId = GlobalConstant.GROUP_ID)
	public void consumeAndUpdate(String message) throws IOException {
		ObjectReader or = new ObjectMapper().readerFor(ProfileDetails.class);
		ProfileDetails json = (ProfileDetails) or.readValue(message);
		profileService.saveProfile(json);
	}

	@KafkaListener(topics = GlobalConstant.DELETE_PROFILE, groupId = GlobalConstant.GROUP_ID)
	public void consumeAndDelete(String message) throws IOException {
		ObjectReader or = new ObjectMapper().readerFor(ProfileDetails.class);
		ProfileDetails json = (ProfileDetails) or.readValue(message);
		profileService.deletProfile(json);
	}

}
