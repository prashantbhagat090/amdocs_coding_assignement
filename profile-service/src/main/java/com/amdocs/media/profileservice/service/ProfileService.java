package com.amdocs.media.profileservice.service;

import com.amdocs.media.profileservice.model.ProfileDetails;
import com.amdocs.media.profileservice.model.Registerresponse;

public interface ProfileService {

	/**
	 * @param profileDetails
	 * @return Registerresponse Will save profile and return success or failure
	 *         operation
	 */
	public Registerresponse saveProfile(ProfileDetails profileDetails);

	/**
	 * @param profileDetails
	 * @return Registerresponse * Will delete profile and return success or failure
	 *         operation
	 */
	public Registerresponse deletProfile(ProfileDetails profileDetails);

}
