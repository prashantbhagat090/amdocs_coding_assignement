package com.amdocs.media.profileservice.daoImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.amdocs.media.profileservice.constant.GlobalConstant;
import com.amdocs.media.profileservice.dao.ProfileDao;
import com.amdocs.media.profileservice.model.ProfileDetails;
import com.amdocs.media.profileservice.model.Registerresponse;
import com.amdocs.media.profileservice.repository.ProfileRepository;

@Repository
public class ProfileDaoImpl implements ProfileDao {

	private static final Logger LOG = LoggerFactory.getLogger(ProfileDaoImpl.class);

	@Autowired
	private ProfileRepository profileRepository;

	@Override
	public Registerresponse saveProfile(ProfileDetails profileDetails) {

		Registerresponse r = new Registerresponse();
		r.setReason(GlobalConstant.PROFILE_UPDATE_MESSAGE);
		r.setStatus(true);
		try {
			ProfileDetails dbprofileDetails = null;
			try {
				dbprofileDetails = profileRepository.getProfileDetailsByPhoneNumber(profileDetails.getPhone_number());
			} catch (Exception e) {
				LOG.error(e.getMessage());
			}
			if (dbprofileDetails != null) {
				profileDetails.setProfileDetailsId(dbprofileDetails.getProfileDetailsId());
			} else if (profileDetails.getPhone_number() == 0) {
				r.setReason(GlobalConstant.PLEASE_PASS_CORRECT_PHONE_NO);
				r.setStatus(false);
				return r;
			}
			profileRepository.save(profileDetails);
		} catch (Exception e) {
			LOG.error(e.getMessage());
			r.setReason(GlobalConstant.ERROR_OCCURED);
			r.setStatus(false);
		}
		return r;

	}

	@Override
	public Registerresponse deletProfile(ProfileDetails profileDetails) {
		Registerresponse r = new Registerresponse();
		r.setReason(GlobalConstant.PROFILE_DELETE_MESSAGE);
		r.setStatus(true);
		try {
			ProfileDetails dbprofileDetails = null;
			try {
				dbprofileDetails = profileRepository.getProfileDetailsByPhoneNumber(profileDetails.getPhone_number());
			} catch (Exception e) {
				LOG.error(e.getMessage());
			}
			if (dbprofileDetails != null) {
				profileRepository.delete(dbprofileDetails);
			} else if (profileDetails.getPhone_number() == 0) {
				r.setReason(GlobalConstant.PROFILE_DOES_NOT_EXIST);
				r.setStatus(false);
				return r;
			}
		} catch (Exception e) {
			r.setReason(GlobalConstant.ERROR_OCCURED);
			r.setStatus(false);
			LOG.error(e.getMessage());
		}
		return r;
	}

}
