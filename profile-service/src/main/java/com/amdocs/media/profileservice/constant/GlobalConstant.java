package com.amdocs.media.profileservice.constant;

public class GlobalConstant {

	public static final String UPDATE_PROFILE = "update_profile";
	public static final String GROUP_ID = "group_id";
	public static final String DELETE_PROFILE = "delete_profile";
	public static final String STAR = "*";
	public static final String SLASH = "/";
	public static final String PROFILE_URL = "profile";
	public static final String PROFILE_UPDATE_MESSAGE = "Profile has been updated successfully!";
	public static final String PROFILE_DELETE_MESSAGE = "Profile has been deleted successfully!";
	public static final String PLEASE_PASS_CORRECT_PHONE_NO = "Please pass correct phone no";
	public static final String ERROR_OCCURED = "Some error has been occured";
	public static final String PROFILE_DOES_NOT_EXIST = "Profile does not exist";
	public static final String PROFILE_DETAILS = "profile_details";
	public static final String ADDRESS = "address";
	public static final String PHONE_NUMBER = "phone_number";
	public static final String GET_PROFILE_BY_PHONE_NUMBER_QUERY = "select * from profile_details where phone_number=:phone_number";
	public static final String REACHED_UPDATE_PROFILE = "Reached upate profile method";
	public static final String REACHED_PUT_PROFILE = "Reached put profile method";
	public static final String REACHED_DELETE_PROFILE = "Reached delete profile method";
	 
}
