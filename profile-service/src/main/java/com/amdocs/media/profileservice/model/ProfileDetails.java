package com.amdocs.media.profileservice.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.amdocs.media.profileservice.constant.GlobalConstant;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@Table(name = GlobalConstant.PROFILE_DETAILS)
public class ProfileDetails implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long profileDetailsId;

	@Column(name = GlobalConstant.ADDRESS)
	private String address;

	@Column(name = GlobalConstant.PHONE_NUMBER)
	private long phone_number;

}
