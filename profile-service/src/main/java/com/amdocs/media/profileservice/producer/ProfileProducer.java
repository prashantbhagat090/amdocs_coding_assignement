package com.amdocs.media.profileservice.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class ProfileProducer {

	@Autowired
	private KafkaTemplate<String, String> kafkaTemplate;

	public void send(String message, String kafkaTopic) {
		kafkaTemplate.send(kafkaTopic, message);
	}
}
