package com.amdocs.media.profileservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.amdocs.media.profileservice.constant.GlobalConstant;
import com.amdocs.media.profileservice.model.ProfileDetails;

@Repository
public interface ProfileRepository extends JpaRepository<ProfileDetails, Integer> {

	@Query(value = GlobalConstant.GET_PROFILE_BY_PHONE_NUMBER_QUERY, nativeQuery = true)
	ProfileDetails getProfileDetailsByPhoneNumber(@Param(GlobalConstant.PHONE_NUMBER) Long phone_number);

}
