package com.amdocs.media.profileservice.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.amdocs.media.profileservice.dao.ProfileDao;
import com.amdocs.media.profileservice.model.ProfileDetails;
import com.amdocs.media.profileservice.model.Registerresponse;
import com.amdocs.media.profileservice.service.ProfileService;

@Service
public class ProfileServiceImpl implements ProfileService {

	@Autowired
	private ProfileDao profileDao;

	@Override
	public Registerresponse saveProfile(ProfileDetails profileDetails) {
		return profileDao.saveProfile(profileDetails);

	}

	@Override
	public Registerresponse deletProfile(ProfileDetails profileDetails) {
		return profileDao.deletProfile(profileDetails);
	}

}
